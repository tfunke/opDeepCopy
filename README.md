
### Functionality  
  
Provide a deep_copy functionality for a given workpackage and its childs and relations in OpenProject using the API.  

You can define either a start date or an end date for the copy. The new workpackage and it´s childs will be timed accordingly. Further on you may introduce placeholders in the strings of customFields and subjects. These can be replaced by values that you hand over for the copies of the workpackage.  
  
This makes it possible to define a template workpackage with all it´s childs and the relations among each other. It can be the base for a number of similar copies in a kind of a rollout project.  
  
### Usage  
  
First generate an instance of OpenProjectWP with the ID of the workpackage to be copied and the connection details to the API.  
  
```python
    from op_wp import OpenProjectWP

    dp = OpenProjectWP(
        workpackage_id,
        con = None     
        apikey=(
        "apikey",
        "your_api_key_to_your_openproject_instance",
        ),
        base_url="http://openproject.example.com",
    )
```
Arguments:  
  * workpackage_id: id of the workpackage to be copied (type: integer)  
  * con (optional, alternative to apikey + base_url): connection to an openproject instance (type: OpenProjectAPIConnect)  
  * apikey: tuple containing the keyword "apikey" and your apikey. Please refer to the Openproject documentation to see, how to generate the key. (type: tuple of strings)  
  * base_url: url to your openproject instance (type: string)

Copy the workpackage by calling:
```python
    dc = dp.deep_copy(target=target)
```
The parameters for the copy are defined in the dictionary target:  

```python
    target = {
        "variables": {
            "<PHASE_NAME>": "Phase Name",
            "<CUSTOM>": "Custom Field",
        },
        "project": {"id": 0, "name": project_name},
        "date": {
            "date":"2020-01-05"
            ),
            "type": "start",
        },
    }
```
All keys in this dictionary and the dictionary itself are optional. By default an exact copy of the original workpackage will be generated. You can change this by defining target as follows:  
  * "variables": dictionary of the placeholders that you define. Each key in the dict will be replaced by its value (must be string) in the following fields of the workpackage and its childs.
    * subject
    * all customfields that you define  
    NOTE: the key can be any string that you define. Should be something that usually will not appear in the text. Otherwise it will be replaced.  

  * "project": dictionary that defines the target project for the new workpackages. By default the workpackage will be generated in the same project like the origin. If defined it needs to have the following key/value pairs:
    * "id": id (type: integer) of the project the new workpackages will be generated in. Must 0 if the project is not yet existing or the id is not known.
    * "name": the name of the target project. Will be generated if not already existing. (type: string)

  * "date": dictionary with the key/values:
    * "date": date string in the format %Y-%m-%d
    * "type": either the string "start" or "end" defining if the given date is the start or the due date of the new workpackage

  
### Example  
Please have a look at the example import_project_dates.py.
  
#### Prerequisits
To setup this example you need to clone this repo:  
````
git clone git@gitlab.com:tfunke/opDeepCopy.git op_test  
cd op_test
`````
Build the Python environment.  
````
python3 -m venv op_test
````
Activate the environment.
On Windows, run:
````
op_test\Scripts\activate.bat
````
On Unix or MacOS, run:
````
source op_test/bin/activate
````
Install the required packages:
````
pip install -r pip_requirements.txt
```` 
  
#### Using your own OpenProject installation

You need to replace the connection details and the apikey with the appropriate settings of your OpenProject installation in import_project_dates.py.  
```python
con = OpenProjectAPIConnect(
    apikey=(
        "apikey",
        "your_own_api_key",
    ),
    base_url="http://openproject.example.com",
)
````

Running import_project_dates.py first creates an example template of a project phase with some placeholders. (Project name: 'DeepCopy Template').   
````
python import_project_dates.py
````
  
You will be asked if you want to proceed giving you the opportunity to first have a look at the template.

#### Using the Docker image  
  
If you have docker installed you might want to run the example by using the docker image already containing the template:

```
docker run -p 8080:80 --rm -e SECRET_KEY_BASE=secret registry.gitlab.com/tfunke/opdeepcopy
````
After the container is up and running you can have a look at the example template at:  
http://localhost:8080  
  
Login as: admin  
Password: openproject  
  
![alt text](images/template.png)  
   
Run import_project_dates.py with the default api connection details:
````
python import_project_dates.py
````
#### Result
After finishing import_project_dates.py you will see two new projects in the OpenProject instance:  
  * "Project based on Start Dates"
  * "Project based on End Dates"  
Both have 3 copies of the template workpackage with the parameters read from the excel file: "new_project.xlsx".

![alt text](images/project.png)
  
This works fine for the given example and the use cases I had so far. Nevertheless it´s not heavily tested and there is probably still bugs and room for optimisation. Please be careful to use this in production! Any comment or support is very welcome.
  

