# -*- coding: utf-8 -*-
import requests
import re
import copy
import datetime as dt


class OpenProjectAPIConnect:
    """connect to the API of an OpenProject instance"""

    def __init__(
        self,
        apikey=("apikey", "1234567890abcdef"),
        base_url="http://openproject.example.com",
        api="/api/v3",
    ):
        """
        initiate the basic parameters for connectivity and
        authentication
        """
        self.authAdmin = apikey
        self.api = api
        self.verify = False
        self.header = {"content-type": "application/json"}
        self.base_url = base_url


class OpenProjectWP(OpenProjectAPIConnect):
    """Provide a deep_copy functionality for a given workpackage

    Instantiate a given workpackage. Save all its childs and relations
    in the dict self.tree. Provide the method deep_copy to copy the
    workpackage and all its childs and relations to a given project.
    Dates, customFields and subjects of the generated workpackages
    can be modified thus giving a kind of templating functionality.
    OpenProjectWP extends OpenProjectAPIConnect and bases on this connection.

    PUBLIC METHODS:
    - wp_copy: copy a workpackage with settings provided
    - deep_copy: copy the workpackage with all childs and relations

    VARIABLES:
    -source_id: id of wp instantiated
    -project_id: id of the project of the instantiated wp
    -workpackages: api route to workpackages
    -projects: api route to projects
    -relations: api route to relations
    -tree: dict containing the wp, all childs, relations and ids of the copies
    -start: earliest start date
    -end: latest end date
    """

    def __init__(
        self,
        wp_id,
        con=None,
        apikey=("apikey", "1234567890abcdef"),
        base_url="http://openproject.example.com",
        api="/api/v3",
    ):
        """Initiate the connection to the API of the OpenProject Instance
        and the workpackage (wp) related parameters.
        Get all childs and relations of the wp and write it to the
        dict tree. Find and set the earliest start date and the latest end
        date of the wp and all it´s childs.
        """
        if not isinstance(con, OpenProjectAPIConnect):
            super().__init__(apikey=apikey, base_url=base_url, api=api)
        else:
            self.authAdmin = con.authAdmin
            self.api = con.api
            self.verify = con.verify
            self.header = con.header
            self.base_url = con.base_url

        self.source_id = wp_id
        self.project_id = 0

        self.workpackages = "work_packages"
        self.projects = "projects"
        self.relations = "relations"

        self.tree = {}
        self.relations_list = []

        self._get_childs_rels(parent_id=self.source_id)

        self.start, self.end = self._get_dates()

    @staticmethod
    def _log_error(*messages):
        """Log errors and messages. Currently only prints to standard output"""
        for m in messages:
            print(m)

    def _get_href(self, *specifics):
        """return href in the form: /{self.api}/{specific1}/{specific2}/..."""
        href = self.api
        for s in specifics:
            s = re.sub(self.api, "", s)
            s = re.sub(self.base_url, "", s)
            href += "/" + s
        return href

    def _get_url(self, *specifics):
        """return url: {self.base_url}/{self.api}/{specific1}/{specific2}/.."""
        url = self.base_url + self._get_href(*specifics)
        return url

    @staticmethod
    def _get_id_from_href(href):
        """extract and return the id from a href string as an integer"""
        return int(re.findall(r"\d*$", href)[0])

    def _get_json_resource(self, url=""):
        """
        return the resource from a requests.get(url) as json or False in case
        of an error
        """
        try:
            r = requests.get(url, auth=self.authAdmin, verify=self.verify)
        except TimeoutError as e:
            self._log_error(e.args[0])

        if r.ok:
            return r.json()
        else:
            self._log_error(f"HTTP Error Code {r.status_code}", url)
            return False

    def _post_resource(self, url="", params={}):
        """post params to url and return result"""
        try:
            r = requests.post(
                url,
                headers=self.header,
                json=params,
                auth=self.authAdmin,
                verify=self.verify,
            )
        except TimeoutError as e:
            self._log_error(e.args[0])

        if not r.ok:
            self._log_error(f"HTTP Error Code {r.status_code}", url)
        return r

    def _get_wp(self, wp_id):
        """get the workpackage wp_id as json or False in case of an error"""
        return self._get_json_resource(
            self._get_url(self.workpackages, str(wp_id))
        )

    def _get_relations(self, wp_id):
        """
        get the workpackages relations as json or False in case of an error
        """
        return self._get_json_resource(
            self._get_url(self.workpackages, str(wp_id), self.relations)
        )

    def _get_childs_rels(self, parent_id=0):
        """
        Recursively get all childs and its relations of the workpackage
        parent_id.
        Write these workpackages to the dict self.tree and their relations to
        the list self.relations_list.
        This method is called from __init__ and thus builds tree and
        self.relations_list for self.source_id.
        """

        if parent_id == 0:
            parent_id = self.source_id
        self.tree[parent_id] = {"wp": {}, "childs": []}
        wp = self._get_wp(parent_id)
        self.tree[parent_id]["wp"] = wp
        self.project_id = self._get_id_from_href(
            wp["_links"]["project"]["href"]
        )
        rels = self._get_relations(parent_id)["_embedded"]["elements"]
        for r in rels:
            # Beziehung noch nicht aus anderem wp übernommen?
            if r not in self.relations_list:
                # speichere die Beziehung in self.relations_list ab
                self.relations_list.append(r)
        if "children" in wp["_links"].keys():
            for c in wp["_links"]["children"]:
                child_id = self._get_id_from_href(c["href"])
                self.tree[parent_id]["childs"].append(child_id)
                self._get_childs_rels(parent_id=child_id)
        return

    def _get_writables(self, s_0={}, s_1={}):
        """
        Generator method.
        Walk recursively through the json schema of the workpackage and
        yield all writable fields
        """
        for k, v in s_1.items():
            if isinstance(v, dict):
                yield from self._get_writables(s_0=k, s_1=v)
            if isinstance(v, list):
                for l in v:
                    if isinstance(l, dict):
                        yield from self._get_writables(s_1=l)
            if k == "writable" and v:
                yield s_0
        return

    def _get_dates(self):
        """
        return earliest start and latest end date over all workpackages
        in self.tree
        """

        date_format = "%Y-%m-%d"
        earliest = dt.datetime.strptime("9999-12-31", date_format)
        latest = dt.datetime.strptime("1900-01-01", date_format)
        for wp_id in self.tree:
            if "startDate" in self.tree[wp_id]["wp"].keys():
                startDate = dt.datetime.strptime(
                    self.tree[wp_id]["wp"]["startDate"], date_format
                )
            if "dueDate" in self.tree[wp_id]["wp"].keys():
                endDate = dt.datetime.strptime(
                    self.tree[wp_id]["wp"]["dueDate"], date_format
                )
            if startDate < earliest:
                earliest = startDate
            if endDate > latest:
                latest = endDate
        return (earliest, latest)

    def _set_date(self, wp_id=0, target={}):
        """
        Return the dict params containing the startDate and dueDate for the
        copy of wp_id.
        Bases on the workpackage to be copied with the id wp_id and the date
        given in the dict target.
        Take the duration from wp_id and relate it to the overall start and
        end date as in the origin self.source_id.
        target['date'] has the format:
        {'date':{'date': YYYY-MM-DD, 'type': 'start' or 'end'}}
        It defines either the overall start or end date of the copy.
        """

        if wp_id == 0:
            wp_id = self.source_id
        date_format = "%Y-%m-%d"
        params = {}
        if "date" in target.keys():
            date = target["date"]
            if (
                not date
                or wp_id == 0
                or "dueDate" not in self.tree[wp_id]["wp"].keys()
                or "startDate" not in self.tree[wp_id]["wp"].keys()
            ):
                return {}
            length = dt.datetime.strptime(
                self.tree[wp_id]["wp"]["dueDate"], date_format
            ) - dt.datetime.strptime(
                self.tree[wp_id]["wp"]["startDate"], date_format
            )
            if date["type"] == "start":
                diff = (
                    dt.datetime.strptime(
                        self.tree[wp_id]["wp"]["startDate"], date_format
                    )
                    - self.start
                )
                startDate = (
                    dt.datetime.strptime(date["date"], date_format) + diff
                )
                dueDate = startDate + length

            if date["type"] == "end":
                diff = self.end - dt.datetime.strptime(
                    self.tree[wp_id]["wp"]["dueDate"], date_format
                )
                dueDate = (
                    dt.datetime.strptime(date["date"], date_format) - diff
                )
                startDate = dueDate - length

            params["startDate"] = dt.datetime.strftime(startDate, date_format)
            params["dueDate"] = dt.datetime.strftime(dueDate, date_format)

        return params

    def _get_customFields(self, wp_id=0):
        """Generator method. Yield all custom fields in wp"""
        for k in self.tree[wp_id]["wp"]:
            if "customField" in k:
                yield k
        return

    def _get_project_id_by_name(self, name=""):
        """return the id of a project with the name name as string"""

        if name == "":
            return self.project_id

        projects = self._get_json_resource(self._get_url(self.projects))[
            "_embedded"
        ]["elements"]

        for i in range(len(projects)):
            if projects[i]["name"] == name:
                return projects[i]["id"]
        return 0

    def _create_project(self, name=None):
        """
        Create the project with the name name and return the id.
        """
        identifier = name.replace(" ", "_").lower()
        project = {"identifier": identifier, "name": name}
        url = self._get_url(self.projects)
        new = self._post_resource(url=url, params=project)
        if new.ok:
            return new.json()["id"]
        else:
            self._log_error(
                "error while creating project {}. \nMessage: {} ".format(
                    name, new.json()["message"]
                )
            )
            return self.project_id

    def _set_variables(self, wp_id=0, target={}):
        """
        Return the dict params containing the updated subject and customFields
        for the copy of wp_id. Variables defined in target are replaced by its
        values.
        Currently only subject and customFields are scanned.
        In future releases also description shall be supported.
        """
        if wp_id == 0:
            wp_id = self.source_id
        params = {}
        if "variables" in target.keys():
            for item in ["subject", *self._get_customFields(wp_id=wp_id)]:
                if self.tree[wp_id]["wp"][item]:
                    s = self.tree[wp_id]["wp"][item]
                    for key, value in target["variables"].items():
                        s = s.replace(key, value)
                    params[item] = s
        return params

    def _set_project(self, project={}):
        """
        Return the dict params containing the project id for the copy.
        """

        params = {}
        if project:
            if int(project["id"]) != 0:
                params["project"] = int(project["id"])
            if project["name"]:
                params["project"] = self._get_project_id_by_name(
                    name=project["name"]
                )
                if params["project"] == 0:
                    params["project"] = self._create_project(
                        name=project["name"]
                    )
        return params

    def wp_copy(self, source_id=0, setting={}, child_of=0):
        """
        Copy the workpackage source_id and replace the parameters by setting
        where given.
        Parameters of source_id are read from self.tree[source_id]['wp']
        Requires the id of the parent workpackage in child_of if exists.
        """

        if source_id == 0:
            source_id = self.source_id
        if self.tree[source_id].get("copy", 0) != 0:
            self._log_error(f"copy of {source_id} already exists")
            return False

        wp = self.tree[source_id]["wp"]
        params = copy.deepcopy(setting)
        params["_links"] = setting.setdefault("_links", {})
        parent = child_of
        schema_ref = wp["_links"]["schema"]["href"]
        schema = self._get_json_resource(self._get_url(schema_ref))
        for k in self._get_writables(s_1=schema):
            if k not in setting:
                if k not in wp["_links"]:
                    params[k] = wp[k]
                else:
                    if wp["_links"][k]["href"]:
                        params["_links"][k] = params.setdefault(
                            k, wp["_links"][k]
                        )
        for k in wp:
            if "customField" in k:
                params[k] = params.setdefault(k, wp[k])

        if parent != 0:
            params["parent"] = {
                "href": self._get_href(self.workpackages, str(parent))
            }
            params["_links"]["parent"] = params["parent"]

        url = self._get_url(
            self.projects, str(params["project"]), self.workpackages
        )

        if "project" in params.keys():
            params.pop("project")
        if "project" in params["_links"].keys():
            params["_links"].pop("project")

        new = self._post_resource(url=url, params=params)
        if new.ok:
            self.tree[wp["id"]]["copy"] = new.json()["id"]
            return True
        else:
            self._log_error(
                "error while copying workpackage {}. \nMessage: {} ".format(
                    source_id, new.json()["message"]
                )
            )
            return False

    def _rel_copy(self):
        """

        """

        ok = True
        for r_orig in self.relations_list:
            r_dest = {}
            # if copy does not exist build relation to the
            # existing workpackage
            for rk in ["from", "to"]:
                rwp_href = r_orig["_links"][rk]["href"]
                rwp = self._get_id_from_href(rwp_href)
                if rwp not in self.tree.keys():
                    r_dest[rk] = rwp_href
                else:
                    rwp_c = self.tree[rwp].get("copy", rwp)
                    r_dest[rk] = self._get_href(self.workpackages, str(rwp_c))

            rel_json = {
                "type": r_orig["type"],
                "_links": {
                    "from": {"href": r_dest["from"]},
                    "to": {"href": r_dest["to"]},
                },
            }
            url = self._get_url(r_dest["from"], self.relations)

            if not self._post_resource(url=url, params=rel_json).ok:
                self._log_error(
                    "Error copying relation: from {} \
                        to {}, type: {}".format(
                        self._get_id_from_href(
                            r_orig["_links"]["from"]["href"]
                        ),
                        self._get_id_from_href(r_orig["_links"]["to"]["href"]),
                        r_orig["type"],
                    )
                )
                ok = False
        return ok

    def _cleanup(self):
        """Clean self.tree for allowing further copies of self.source_id"""
        for wp_id in self.tree:
            self.tree[wp_id]["copy"] = 0

    def deep_copy(self, target={}):

        for wp_id in self.tree:
            c = True
            setting = {}
            setting["project"] = self.project_id
            if "project" in target.keys():
                setting.update(self._set_project(target["project"]))
            if self.tree[wp_id].get("copy", 0) == 0:
                setting.update(self._set_variables(wp_id=wp_id, target=target))
                setting.update(self._set_date(wp_id=wp_id, target=target))
                c = self.wp_copy(source_id=wp_id, setting=setting)
                if not c:
                    return False
            for child_id in self.tree[wp_id]["childs"]:
                setting = {}
                setting["project"] = self.project_id
                if "project" in target.keys():
                    setting.update(self._set_project(target["project"]))
                setting.update(self._set_date(wp_id=child_id, target=target))
                setting.update(
                    self._set_variables(wp_id=child_id, target=target)
                )
                c = self.wp_copy(
                    source_id=child_id,
                    child_of=self.tree[wp_id]["copy"],
                    setting=setting,
                )
                if not c:
                    return False
        if not self._rel_copy():
            self._log_error("Not all relations could be copied")

        dc = OpenProjectWP(
            self.tree[self.source_id].get("copy", 0),
            apikey=self.authAdmin,
            base_url=self.base_url,
            api=self.api,
        )
        self._cleanup()
        return dc
