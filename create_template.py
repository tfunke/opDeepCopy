# -*- coding: utf-8 -*-
"""
Create an example template of a project phase with child
workpackages and relations in the project "DeepCopy Template".

<--------------phase_1-------------->
<--task_1-->
            |
            m_1
            |
            <--------task_2-------->
            <-task_21->             |
                       <--task_22-->
                                    |
                                    m_2

Created on Fri Dec  06 2019
@author: Thomas.Funke
"""

import requests


def create_template(con):
    """create the template via the connection 'con'"""

    def _get_project_id_by_name(name=""):
        """return the id of a project with the name name as string"""

        if name == "":
            return False
        url = con.base_url + f"/api/v3/projects/"
        projects = requests.get(
            url, headers=con.header, auth=con.authAdmin, verify=con.verify
        ).json()["_embedded"]["elements"]

        for i in range(len(projects)):
            if projects[i]["name"] == name:
                return projects[i]["id"]
        return False

    wp = {}
    p = "<PHASE_NAME>"
    c = "<CUSTOM>"
    project_name = "DeepCopy Template"

    # check if template already exists
    p_id = _get_project_id_by_name(name=project_name)
    if p_id:
        url = con.base_url + f"/api/v3/projects/{p_id}/work_packages"
        r = requests.get(
            url, headers=con.header, auth=con.authAdmin, verify=con.verify
        )
        # return id from phase_1
        if r.json()["_embedded"]["elements"][0]["subject"] == f"Phase: {p}":
            return r.json()["_embedded"]["elements"][0]["id"]
        else:
            return "Please delete existing Template!"

    # create template
    wp["phase_1"] = {"parent": "", "id": "", "relations": []}
    wp["task_1"] = {"parent": "phase_1", "id": "", "relations": []}
    wp["task_2"] = {
        "parent": "phase_1",
        "id": "",
        "relations": [
            {
                "type": "follows",
                "_links": {"from": {"href": "task_2"}, "to": {"href": "m_1"}},
            }
        ],
    }
    wp["task_21"] = {"parent": "task_2", "id": "", "relations": []}
    wp["task_22"] = {
        "parent": "task_2",
        "id": "",
        "relations": [
            {
                "type": "follows",
                "_links": {
                    "from": {"href": "task_22"},
                    "to": {"href": "task_21"},
                },
            }
        ],
    }
    wp["m_1"] = {
        "parent": "phase_1",
        "id": "",
        "relations": [
            {
                "type": "follows",
                "_links": {"from": {"href": "m_1"}, "to": {"href": "task_1"}},
            }
        ],
    }
    wp["m_2"] = {
        "parent": "phase_1",
        "id": "",
        "relations": [
            {
                "type": "follows",
                "_links": {"from": {"href": "m_2"}, "to": {"href": "task_2"}},
            }
        ],
    }

    wp["phase_1"]["json"] = {
        "subject": f"Phase: {p}",
        "description": {"format": "markdown", "raw": "", "html": ""},
        "_links": {
            "type": {"href": "/api/v3/types/3"},
            "status": {"href": "/api/v3/statuses/1"},
            "priority": {"href": "/api/v3/priorities/8"},
            "parent": {"href": None},
        },
    }

    wp["task_1"]["json"] = {
        "subject": f"task 1 in {p}",
        "description": {"format": "markdown", "raw": "", "html": ""},
        "startDate": "2019-12-24",
        "dueDate": "2019-12-25",
        "customField1": f"{c} in {p}",
        "_links": {
            "type": {"href": "/api/v3/types/1"},
            "status": {"href": "/api/v3/statuses/1"},
            "priority": {"href": "/api/v3/priorities/8"},
            "parent": {"href": None},
        },
    }

    wp["task_2"]["json"] = {
        "subject": f"task 2 in {p}",
        "description": {"format": "markdown", "raw": "", "html": ""},
        "customField1": f"{c} in {p}",
        "_links": {
            "type": {"href": "/api/v3/types/1"},
            "status": {"href": "/api/v3/statuses/1"},
            "priority": {"href": "/api/v3/priorities/8"},
            "parent": {"href": None},
        },
    }

    wp["task_21"]["json"] = {
        "subject": f"task 2_1",
        "description": {"format": "markdown", "raw": "", "html": ""},
        "startDate": "2019-12-26",
        "dueDate": "2019-12-27",
        "customField1": f"{c} in {p}",
        "_links": {
            "type": {"href": "/api/v3/types/1"},
            "status": {"href": "/api/v3/statuses/1"},
            "priority": {"href": "/api/v3/priorities/8"},
            "parent": {"href": None},
        },
    }

    wp["task_22"]["json"] = {
        "subject": f"task 2_2",
        "description": {"format": "markdown", "raw": "", "html": ""},
        "startDate": "2019-12-30",
        "dueDate": "2019-12-31",
        "customField1": f"{c} in {p}",
        "_links": {
            "type": {"href": "/api/v3/types/1"},
            "status": {"href": "/api/v3/statuses/1"},
            "priority": {"href": "/api/v3/priorities/8"},
            "parent": {"href": None},
        },
    }

    wp["m_1"]["json"] = {
        "subject": f"End of task1 in {p}",
        "description": {"format": "markdown", "raw": "", "html": ""},
        "_links": {
            "type": {"href": "/api/v3/types/2"},
            "status": {"href": "/api/v3/statuses/1"},
            "priority": {"href": "/api/v3/priorities/8"},
            "parent": {"href": None},
        },
    }

    wp["m_2"]["json"] = {
        "subject": f"End of phase {p}",
        "description": {"format": "markdown", "raw": "", "html": ""},
        "_links": {
            "type": {"href": "/api/v3/types/2"},
            "status": {"href": "/api/v3/statuses/1"},
            "priority": {"href": "/api/v3/priorities/8"},
            "parent": {"href": None},
        },
    }

    # generate the template project
    # (to create a project via API is realized
    # with OpenProject Feature/projects api write #7647)
    project_id = _get_project_id_by_name(name=project_name)
    if not project_id:
        project = {
            "identifier": "example_template",
            "name": project_name,
            "customField1": {
                "type": "String",
                "name": "customField1",
                "required": False,
                "hasDefault": False,
                "writable": True,
                "visibility": "default",
            },
        }
        url = con.base_url + "/api/v3/projects/"
        r = requests.post(
            url,
            json=project,
            headers=con.header,
            auth=con.authAdmin,
            verify=con.verify,
        )
        project_id = r.json()["id"]

    # create workpackages in order from parents to children
    url = con.base_url + f"/api/v3/projects/{project_id}/work_packages/"
    order = ["phase_1", "task_1", "task_2", "task_21", "task_22", "m_1", "m_2"]
    for k in order:
        parent = wp[k]["parent"]
        if parent:
            wp[k]["json"]["_links"]["parent"]["href"] = (
                con.base_url + "/api/v3/work_packages/" + str(parent)
            )
        r = requests.post(
            url,
            headers=con.header,
            json=wp[k]["json"],
            auth=con.authAdmin,
            verify=con.verify,
        )
        wp[k]["id"] = r.json()["id"]
        href = con.base_url + "/api/v3/work_packages/"
        for task in order:
            if wp[task]["parent"] == k:
                wp[task]["parent"] = wp[k]["id"]
            for i in range(len(wp[task]["relations"])):
                if wp[task]["relations"][i]["_links"]["from"]["href"] == k:
                    wp[task]["relations"][i]["_links"]["from"][
                        "href"
                    ] = href + str(wp[k]["id"])
                if wp[task]["relations"][i]["_links"]["to"]["href"] == k:
                    wp[task]["relations"][i]["_links"]["to"][
                        "href"
                    ] = href + str(wp[k]["id"])
    # create relations
    url = con.base_url + "/api/v3/work_packages/"
    for k in order:
        if wp[k]["relations"]:
            for i in range(len(wp[task]["relations"])):
                r = requests.post(
                    url + str(wp[k]["id"]) + "/relations",
                    headers=con.header,
                    json=wp[k]["relations"][i],
                    auth=con.authAdmin,
                    verify=con.verify,
                )
    return wp["phase_1"]["id"]
