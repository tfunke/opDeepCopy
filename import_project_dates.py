# -*- coding: utf-8 -*-
"""
Example for the usage of op_wp.
First create a template project with nested workpackages and relations.
Read parameters from a spreadsheet and build a
deep copy (including childs and relations)
of this template project phase for each line
in the spreadsheet.
Easiest way to test this example, if no OpenProject installation is available,
is the docker image. Refer to https://www.openproject.org/docker/

Created on Sat Nov  23 2019
@author: Thomas.Funke
"""
from op_wp import OpenProjectWP, OpenProjectAPIConnect
from pandas import read_excel, DataFrame
from datetime import datetime
from create_template import create_template

df = DataFrame()
# target = {
#     "variables": {
#         "<PHASE_NAME>": "phase name",
#         "<CUSTOM>": "custom field value",
#     },
#     "project": {"id": 0, "name": "New Project Name"},
#     "date": {"date": "2019-12-24", "type": "start"},
# }
con = OpenProjectAPIConnect(
    apikey=(
        "apikey",
        "7b33f819c7531ae8ad672be7a8757af79b0674baf85a369a3433b63f7fa9b05c",
    ),
    base_url="https://openproject-ans.axians.de",
)

wp_id = create_template(con)
if wp_id == "Please delete existing Template!":
    print(wp_id)
else:
    response = input(
        "Entering Y creates two projects in your OpenProject instance\n\
        based on the data in new_project.xlsx and the template project\n\
        'DeepCopy Template'.\n\
        One is calculated from the given start dates in the excel and the\n\
        other one bases on the given end dates.\n\
        \n\
        Create example projects from template? (Y/n) "
    )
    if response == "Y" or "y":
        dp = OpenProjectWP(wp_id, con=con)
        try:
            df = read_excel("new_project.xlsx")
            # build copies of the template by using the parameters
            # read from new_project.xlsx
            project_name = "Project based on Start Dates"
            for i in df.index:
                target = {
                    "variables": {
                        "<PHASE_NAME>": df.loc[i, "Phase Name"],
                        "<CUSTOM>": df.loc[i, "Custom Field"],
                    },
                    "project": {"id": 0, "name": project_name},
                    "date": {
                        "date": datetime.strftime(
                            df.loc[i, "Start Date"], format="%Y-%m-%d"
                        ),
                        "type": "start",
                    },
                }
                dc = dp.deep_copy(target=target)
                if dc:
                    print(f"Created {df.loc[i, 'Phase Name']} in {project_name}")
            # build the same copies based on the end dates
            project_name = "Project based on End Dates"
            for i in df.index:
                target = {
                    "variables": {
                        "<PHASE_NAME>": df.loc[i, "Phase Name"],
                        "<CUSTOM>": df.loc[i, "Custom Field"],
                    },
                    "project": {"id": 0, "name": project_name},
                    "date": {
                        "date": datetime.strftime(
                            df.loc[i, "End Date"], format="%Y-%m-%d"
                        ),
                        "type": "end",
                    },
                }
                dc = dp.deep_copy(target=target)
                if dc:
                    print(f"Created {df.loc[i, 'Phase Name']} in {project_name}")
        except FileNotFoundError:
            print("'new_project.xlsx' not found")
    else:
        print("done")
